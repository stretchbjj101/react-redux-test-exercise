const ItemList = {
    "songs": [
        {
            "id": 1,
            "artist": "Ed Sheeran",
            "title": "I don't care",
            "price": 1.50
        },
        {
            "id": 2,
            "artist": "Stormzy",
            "title": "Crown",
            "price": 2.00
        },
        {
            "id": 3,
            "artist": "Lewis Capaldi",
            "title": "Someone you Loved",
            "price": 1.25
        }
    ]
}

export default ItemList;