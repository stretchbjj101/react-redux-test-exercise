import { connect } from 'react-redux';
import ViewItems from '../components/ViewItems';
import { addItem, removeItem } from '../store/basket'

const mapStateToProps = (state) => ({
    basketList: state.basket.itemList 
  });
  
const mapDispatchToProps = {
    addItem,
    removeItem
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewItems);