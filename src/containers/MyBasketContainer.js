import { connect } from 'react-redux';
import MyBasket from '../components/MyBasket';

const mapStateToProps = (state) => ({
    itemList: state.basket.itemList 
  });
  
  const mapDispatchToProps = {
    
  };

export default connect(mapStateToProps, mapDispatchToProps)(MyBasket);