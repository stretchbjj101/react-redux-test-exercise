import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import HeaderContainer from './containers/HeaderContainer';
import HomeContainer from './containers/HomeContainer';
import ViewItemsContainer from './containers/ViewItemsContainer';
import MyBasketContainer from './containers/MyBasketContainer';

function App() {
  return (
    <div className="App">
      <HeaderContainer />
      <Switch>
        <Route exact path="/" component={HomeContainer} />
        <Route exact path="/items" component={ViewItemsContainer} />
        <Route exact path="/my-basket" component={MyBasketContainer} />
      </Switch>
    </div>
  );
}

export default App;
