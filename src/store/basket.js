import { ADD_ITEM, REMOVE_ITEM } from "./actionTypes";

export function addItem(item) {
    return {
        type: ADD_ITEM,
        item
    }
}

export function removeItem(itemId) {
    return {
        type: REMOVE_ITEM,
        itemId
    }
}

const initialState = {
    itemList: []
}

export default function basketReducer(state = initialState, action) {
    switch (action.type) {  
        case ADD_ITEM:
            return {
                ...state,
                itemList: [...state.itemList, action.item]
            }
        case REMOVE_ITEM:
            return {
                ...state,
                itemList: [...state.itemList].filter((value, index, arr) => 
                    value.id !== action.itemId
                )
            }
        default:
                return state;
    }   
}