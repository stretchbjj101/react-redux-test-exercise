import basketReducer, * as basket from './basket'
import { ADD_ITEM, REMOVE_ITEM } from './actionTypes';

/***************
 * Action tests
 **************/

describe('actions', () => {
    it('should create an action for an add item', () => {
        const item = {
            "id": 1,
            "artist": "Test",
            "title": "TEST",
            "price": 1.50
        };
        const expected = {
          type: ADD_ITEM,
          item
        };
    
        expect(basket.addItem(item)).toEqual(expected);
      });

      it('should create an action for an remove item', () => {
        const item = {
            "id": 1,
            "artist": "Test",
            "title": "TEST",
            "price": 1.50
        };
        const expected = {
          type: REMOVE_ITEM,
          itemId: item.id
        };
        
        expect(basket.removeItem(item.id)).toEqual(expected);
      });

})

/****************
 * Reducer tests
 ****************/

describe('reducer', () => {
    it('should return the initial state', () => {
        expect(basketReducer(undefined, {})).toEqual({
            itemList: []
        })
    })

    it('should handle add item', () => {
        const state = {
            itemList: []
        }

        const item = {
            "id": 1,
            "artist": "Test",
            "title": "TEST",
            "price": 1.50
        };

        expect(basketReducer(state, {type: ADD_ITEM, item})).toEqual({
            itemList: [item]
        })
    })

    it('should handle remove item', () => {
        const item = {
            "id": 1,
            "artist": "Test",
            "title": "TEST",
            "price": 1.50
        };

        const state = {
            itemList: [item]
        }

        expect(basketReducer(state, {type: REMOVE_ITEM, itemId: item.id})).toEqual({
            itemList: []
        })
    })

})