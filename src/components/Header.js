import React from "react";
import { Menu } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const Header = () => (
    <div className="main-header">
        <h1 className="title">My Application</h1>
        <Menu stackable pointing size="small" className='main-menu'>
          <Menu.Item header>
            <NavLink to='/'>
              Home
            </NavLink>
          </Menu.Item>
          <Menu.Item header>
            <NavLink to='/items'>
              View Items
            </NavLink>
          </Menu.Item>
          <Menu.Item header>
            <NavLink to='/my-basket'>
              My Basket
            </NavLink>
          </Menu.Item>
        </Menu>
    </div>
    
  );

export default Header;