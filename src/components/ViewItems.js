import React from "react";
import ItemList from '../item-list'
import { Button } from 'semantic-ui-react'

class ViewItems extends React.Component {

    render() {
        return (
            <div>
            <h1>Items</h1>
            <ul className="song-list">
                {ItemList.songs.map(item => <li key={item.id}>
                    <h2>Artist</h2>
                    {item.artist}
                    <h2>Title</h2>
                    {item.title}
                    <p><b>PRICE</b> £ {item.price.toFixed(2)}</p>
                    <div>
                    {
                        (this.props.basketList.includes(item) && <Button color="green" onClick={() => {this.props.removeItem(item.id)}}>Remove from Basket</Button>) ||
                        <Button onClick={() => {this.props.addItem(item)}}>Add Item</Button>
                    }
                    </div>
                </li>)}
            </ul>
            </div>
        )
    }
}

export default ViewItems;