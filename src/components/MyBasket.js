import React from "react";

class MyBasket extends React.Component {
    
    render() {
        return (
            <div>
                <h1>My Basket</h1>
                <ul>
                    {this.props.itemList.map(item => <li key={item.id}>
                        {item.artist} - {item.title}
                    </li>)}
                </ul>
                <h2>Total Cost</h2>
                <div>
                    £
                    {
                        totalCost(this.props.itemList).toFixed(2)
                    }
                </div>
            </div>
        )
    }
}

function totalCost(items) {
    let total = 0;
    items.forEach(element => {
        total += element.price
    });
    return total;
}

export default MyBasket;